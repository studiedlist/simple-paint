This project is simple paint program which is able to:

- draw circles with `left mouse button`
- draw lines with `right mouse button`
- draw spirals with `middle mouse button`
- change color with `mouse scroll`
- switch between filled/unfilled circle drawing mode with `spacebar`

![](example.png)
