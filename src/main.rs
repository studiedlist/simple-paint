extern crate minifb;

use std::cmp::Ordering;
use minifb::{Key, KeyRepeat, Window, WindowOptions, MouseButton, MouseMode};

const WIDTH: usize = 800;
const HEIGHT: usize = 600;

fn main() {
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];
    let mut paint_buffer : Vec<u32> = vec![0; WIDTH * HEIGHT];
    let mut start_coords : Option<(usize, usize)>= None;
    let mut color : u32 = 255;
    let mut color_changed = true;
    let mut draw_filled = false;

    let mut window = Window::new(
        "Paint",
        WIDTH,
        HEIGHT,
        WindowOptions::default(),
    ).unwrap();

    window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));

    while window.is_open() && !window.is_key_down(Key::Escape) {
        let get_mouse_down = || {
            match (window.get_mouse_down(MouseButton::Left), window.get_mouse_down(MouseButton::Right), window.get_mouse_down(MouseButton::Middle)) {
                (true, true, true) => None,
                (true, _, _) => Some(MouseButton::Left),
                (_, true, _) => Some(MouseButton::Right),
                (_, _, true) => Some(MouseButton::Middle),
                (_, _, _) => None,
            }
        };
        let get_mouse_pos = || {
            let pos = window.get_mouse_pos(MouseMode::Discard)?;
            Some((pos.0 as usize, pos.1 as usize))
        };
        if window.is_key_pressed(Key::Backspace, KeyRepeat::No) {
            buffer.fill(0);
            color_changed = true;
        }
        if color_changed {
            if draw_filled {
                for x in (WIDTH - WIDTH / 15)..WIDTH {
                    for y in 0..=(HEIGHT / 20) {
                        set_pixel(&mut buffer, (x, y), color);
                    }
                }
            } else {
                for x in (WIDTH - WIDTH / 15)..WIDTH {
                    for y in 0..=(HEIGHT / 20) {
                        if x > (WIDTH - WIDTH / 15) && y > 0 && x < WIDTH - 1 && y < HEIGHT / 20 {
                            set_pixel(&mut buffer, (x, y), 0);
                        } else {
                            set_pixel(&mut buffer, (x, y), color);
                        }
                    }
                }
            }
            color_changed = false;
        }
        if window.is_key_pressed(Key::Space, KeyRepeat::No) {
            draw_filled = !draw_filled;
            color_changed = true;
        }
        if let Some((_, scroll)) = window.get_scroll_wheel() {
            let mut c = to_u8_rgb(color);
            color_changed = true;
            if scroll > 0.0 {
                let scroll = scroll as u32 * 10;
                c.0 += 30 + (scroll % 255) as u8 / 3;
                c.1 += 30 + (scroll % 255) as u8 / 2;
                c.2 += 30 + (scroll % 255) as u8;
                color = from_u8_rgb(c);
            } else {
                color -= (scroll * -10.0) as u32;
            }
        }
        let buffer = match (get_mouse_down(), get_mouse_pos(), start_coords) {
            (Some(_), Some(coords), None) => {
                start_coords = Some(coords);
                &buffer
            }, 
            (Some(button), Some(to), Some(from)) => {
                let coords = match button {
                    MouseButton::Right => get_line_coords(from, to),
                    MouseButton::Left => {
                        let distance = calculate_distance(from, to);
                        let coords = match draw_filled {
                            true => get_filled_circle_coords(distance),
                            false => get_circle_coords(distance),
                        };
                        let cx = from.0 as i32;
                        let cy = from.1 as i32;
                        coords.iter()
                            .map(|(x, y)| (x + cx, y + cy))
                            .map(|(x, y)| (x as usize, y as usize))
                            .collect()
                    }
                    MouseButton::Middle => {
                        let distance = calculate_distance(from, to);
                        let coords = get_spiral_coords(distance);
                        let cx = from.0 as i32;
                        let cy = from.1 as i32;
                        coords.iter()
                            .map(|(x, y)| (x + cx, y + cy))
                            .map(|(x, y)| (x as usize, y as usize))
                            .collect()
                    },
                };
                paint_buffer = buffer.clone();
                let set_pixel = |&coords| set_pixel(&mut paint_buffer, coords, color);
                coords.iter().for_each(set_pixel);
                &paint_buffer
            }
            (_, _, Some(_)) => {
                start_coords = None;
                buffer = paint_buffer.clone();
                &buffer
            },
            (_, _, _) => &buffer,
        };

        window
            .update_with_buffer(buffer, WIDTH, HEIGHT)
            .unwrap();
    }
}

fn set_pixel(buffer: &mut Vec<u32>, (x, y): (usize, usize), color: u32) {
    if x >= WIDTH || y >= HEIGHT {
        return
    }
    let index = WIDTH * y + x;
    buffer[index] = color;
}

pub fn abs_diff(x: usize, y: usize) -> usize {
    match x.cmp(&y) {
        Ordering::Greater => x - y,
        Ordering::Less => y - x,
        Ordering::Equal => 0,
    }
}

pub fn max(x: usize, y: usize) -> usize {
    if x > y {
        x
    } else {
        y 
    }
}

pub fn min(x: usize, y: usize) -> usize {
    if x > y {
        y
    } else {
        x
    }
}

pub fn calculate_distance(from: (usize, usize), to: (usize, usize)) -> usize {
    if from.0 == to.0 {
        return abs_diff(from.1, to.1)
    }
    if from.1 == to.1 {
        return abs_diff(from.0, to.0)
    }
    let a = abs_diff(to.1, from.1);
    let b = abs_diff(to.0, from.0);
    let dist = (a * a + b * b) as f32;
    let dist = dist.sqrt();
    dist as usize
}

pub fn get_spiral_coords(r: usize) -> Vec<(i32, i32)> {
    (0..(r * 10))
        .map(|angle| {
            let angle = angle as f32 / 100.0;
            let a_x = (angle * (angle / 2.0) * (angle).cos().to_degrees() / 200.0) as i32;
            let a_y = (angle * (angle / 2.0) * (angle).sin().to_degrees() / 200.0) as i32;
            (a_x, a_y)
        })
        .collect()
}

pub fn get_filled_circle_coords(r: usize) -> Vec<(i32, i32)> {
    let mut circle = vec![];
    let (sx, sy) = (0, 0);
    let ex = r * 2;
    let ey = ex;
    let ri = r as i32;
    for x in sx..=ex {
        for y in sy..=ey {
            if calculate_distance((x, y), (r, r)) < r {
                let (x, y) = (x as i32 - ri, y as i32 - ri);
                circle.push((x, y));
            }
        }
    }
    circle
}

pub fn get_circle_coords(r: usize) -> Vec<(i32, i32)> {
    let mut coords = vec![];
    let (mut x, mut y) = (0, r as i32);
    let mut d = 3 - 2 * r as i32;
    while y >= x {
        if d > 0 {
            y -= 1;
            d = d + 4 * (x - y) + 10;
        } else {
            d = d + 4 * x + 6;
        }
        coords.push((x, y));
        coords.push((0 - x, y));
        coords.push((x, 0 - y));
        coords.push((0 - x, 0 - y));
        coords.push((y, x));
        coords.push((0 - y, x));
        coords.push((y, 0 - x));
        coords.push((0 - y, 0 - x));
        x += 1;
    }
    coords
}

pub fn get_line_coords((sx, sy): (usize, usize), (ex, ey): (usize, usize)) -> Vec<(usize, usize)>{
    let mut result = vec![];
    let dx = abs_diff(ex, sx) as i32;
    let dy = abs_diff(ey, sy) as i32;
    let d = dx < dy;
    let swap_if = |(a, b), c| {
        match c {
            true => (b, a),
            false => (a, b),
        }
    };
    let swap_d = |t| swap_if(t, dx < dy);
    let (mut sx, mut sy) = swap_d((sx as i32, sy as i32));
    let (ex, ey) = swap_d((ex as i32, ey as i32));
    let (dx, dy) = swap_d((dx, dy));
    let mut pk : i32 = 2 * dy - dx;
    for _ in 0..=dx {
        match sx < ex {
            true => sx += 1,
            false => sx -= 1,
        }
        match pk < 0 {
            true => pk += 2 * dy,
            false => {
                match sy < ey {
                    true => sy += 1,
                    false => sy -= 1,
                }
                pk = pk + 2 * dy - 2 * dx;
            }
        }
        match d {
            true => result.push((sy as usize, sx as usize)),
            false => result.push((sx as usize, sy as usize)),
        }
    }
    result
}

fn from_u8_rgb((r, g, b): (u8, u8, u8)) -> u32 {
    let (r, g, b) = (r as u32, g as u32, b as u32);
    (r << 16) | (g << 8) | b
}

fn to_u8_rgb(color: u32) -> (u8, u8, u8) {
    ((color >> 16) as u8 % 255, (color << 8) as u8 % 255, (color % 255) as u8)
}
